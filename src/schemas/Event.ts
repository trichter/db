export interface IRecurringRule {
  units: number|number[]
  measure: 'days'|'weeks'|'months'|'years'|'dayOfWeek'|'dayOfMonth'|'weekOfMonth'|'weekOfYear'|'monthOfYear'|'weekOfMonthByDay'
}

export interface IRecurringOptionsFrontmatter {
  rules: IRecurringRule[]
  end?: Date|string
  exceptions?: Date[]|string[]
}
export interface IRecurringOptions extends IRecurringOptionsFrontmatter {
  end?: Date
  exceptions?: Date[]
}

export interface IEventFrontmatter {
  id: string
  parentId?: string|null
  title: string
  start: Date|string
  end?: Date|string|null
  locationName?: string|null
  address?: string|null
  link?: string|null
  image?: string|null
  teaser?: string|null
  description?: string|null
  recurring?: IRecurringOptionsFrontmatter|null
  isCancelled?: boolean
  isCrawled?: boolean
  isFeatured?: boolean
}

export default interface IEvent extends IEventFrontmatter {
  start: Date
  end?: Date
  recurring?: IRecurringOptions
}
