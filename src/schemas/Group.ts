
export default interface Group {
  key?: string
  name: string
  website: string
  email?: string|null
  address?: string|null
  pgp?: {
    url: string
    fingerprint: string
  }
  scrape?: {
    source: string
    options?: any
    exclude?: string[]
    filter?: {[s: string]: string}
  }
  description?: string
}

export interface IGroupFrontmatter extends Group {}
