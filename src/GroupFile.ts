import { MarkdownFile, ImageFile } from '@trichter/git-db'
import Group from './schemas/Group'
import * as path from 'path'

import 'moment-timezone'

import schema from './schemas/schemas.json'

export default class GroupFile extends MarkdownFile implements Group {
  public static readonly filenameRegex: RegExp = /(^|\/)group.md$/
  key: string
  name: string
  website: string
  email: string
  address: string
  scrape = null
  description: string

  imageFile: ImageFile = null // set in a second step in TrichterDBBranch::loadData()

  get imagePath (): string {
    if (!this.imageFile) return null
    return path.join(this.key, `group${path.extname(this.imageFile.filename)}`)
  }

  constructor (filename: string) {
    super(filename)
    this.useSchema(schema.definitions.IGroupFrontmatter)
  }

  parse (body: any): void {
    super.parse(body)

    // for more covenient usage, we pass all the yaml data to the parent object
    Object.assign(this, this.data)
    this.key = path.dirname(this.filename)
    this.description = this.body
    delete this.data
  }

  toString (): string {
    throw new Error('unimplemented')
  }
}
