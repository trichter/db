import { MarkdownFile, ImageFile } from '@trichter/git-db'
import Event, { IEventFrontmatter, IRecurringOptions } from './schemas/Event'
import moment from 'moment'
import 'moment-timezone'
import GroupFile from './GroupFile'
import schema from './schemas/schemas.json'

export default class EventFile extends MarkdownFile implements Event {
  public static readonly filenameRegex: RegExp = /.*\.md$/
  id: string
  parentId: string
  title: string
  start: Date
  end: Date
  locationName: string
  address: string
  link: string
  image: string
  teaser: string
  recurring: IRecurringOptions
  isCancelled: boolean = null
  isFeatured: boolean = null
  isCrawled: boolean = null
  description: string

  data: IEventFrontmatter

  group: GroupFile = null // set in a second step in TrichterDBBranch::loadData()
  imageFile: ImageFile = null // set in a second step in TrichterDBBranch::loadData()

  constructor (filename: string) {
    super(filename)
    this.useSchema(schema.definitions.IEventFrontmatter)
  }

  parse (body): void {
    super.parse(body)

    // for more covenient usage, we pass all the yaml data to the parent object
    Object.assign(this, this.data)

    // parse Dates
    if (typeof this.start === 'string') this.start = moment(this.start).toDate()
    if (typeof this.end === 'string' && this.end) this.end = moment(this.end).toDate()

    if (this.recurring) {
      const rec = this.recurring
      if (rec.exceptions) rec.exceptions = rec.exceptions.map(e => moment(e).toDate())
    }
    this.description = this.body
    delete this.data
  }

  toString (): string {
    this.body = this.description
    this.data = {
      id: this.id,
      title: this.title,
      start: moment(this.start).format('YYYY-MM-DD HH:mm'),
      end: this.end ? moment(this.end).format('YYYY-MM-DD HH:mm') : null,
      locationName: this.locationName || null,
      address: this.address || null,
      link: this.link || null,
      image: this.image || null,
      teaser: this.teaser || null,
      recurring: this.recurring && this.recurring.rules ? {
        rules: this.recurring.rules,
        end: this.recurring.end ? moment(this.recurring.end).format('YYYY-MM-DD') : null,
        exceptions: this.recurring.exceptions ? this.recurring.exceptions.map(e => moment(e).format('YYYY-MM-DD')) : []
      } : null,
      isCancelled: this.isCancelled !== null ? !!this.isCancelled : null,
      isFeatured: this.isFeatured !== null ? !!this.isFeatured : null,
      isCrawled: this.isCrawled !== null ? !!this.isCrawled : null
    }

    // remove null fields
    for (const key in this.data) {
      if (this.data[key] === null) {
        delete this.data[key]
      }
    }
    return super.toString()
  }
}
