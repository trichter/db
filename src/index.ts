import GitDB, { GitDBBranch, ImageFile, BaseFile, YAMLFile, MarkdownFile } from '@trichter/git-db'
import EventFile from './EventFile'
import GroupFile from './GroupFile'
import moment from 'moment'
import * as path from 'path'
import 'moment-timezone'
import Logger from '@trichter/logger'
export { GitDB, GitDBBranch, BaseFile, YAMLFile, MarkdownFile, ImageFile }

export { default as Group } from './GroupFile'
export { default as Event } from './EventFile'
const log = Logger('trichter-db')

export class TrichterDBBranch extends GitDBBranch {
  events: EventFile[] = []
  groups: GroupFile[] = []
  async loadData (): Promise<void> {
    log.debug(`load files in ${this.branchName}`)
    await super.loadData()
    this.groups = super.get(file => file instanceof GroupFile) as GroupFile[]

    // assign group image
    for (const group of this.groups) {
      const dir = path.dirname(group.filename)
      const imageFileNameJPG = path.join(dir, 'group.jpg')
      const imageFileNamePNG = path.join(dir, 'group.png')
      const images = super.get(file => file instanceof ImageFile && (file.filename === imageFileNameJPG || file.filename === imageFileNamePNG)) as ImageFile[]
      if (images.length) {
        group.imageFile = images[0]
      }
    }
    this.events = super.get(file => file instanceof EventFile) as EventFile[]

    for (const event of this.events) {
      // assign group
      const dir = path.dirname(event.filename)
      event.group = this.groups.find(g => g.key === dir)

      // assign image
      if (event.image) {
        const filename = path.join(dir, 'images', event.image)
        const images = super.get(file => file instanceof ImageFile && (file.filename === filename)) as ImageFile[]
        if (images.length) {
          event.imageFile = images[0]
        }
      }
    }
  }

  async fork (name: string): Promise<TrichterDBBranch> {
    await super.fork(name)
    return new TrichterDBBranch(this.db, name)
  }

  async save (file: BaseFile): Promise<void> {
    await super.save(file)

    // is event? update events array
    if (file instanceof EventFile) {
      const index = this.events.findIndex(e => e.id === file.id && e.group.key === file.group.key)
      if (index !== -1) {
        this.events[index] = file
      } else {
        this.events.push(file)
      }
    }
    // TODO: handle all other file changes
  }
}

export default class TrichterDB extends GitDB {
  constructor (path: string, timezone: string = 'Europe/Berlin') {
    log.debug(`open TrichterDB in ${path}`)
    super(path)
    super.registerFileClass(EventFile)
    super.registerFileClass(GroupFile)
    // multiple TrichterDB instances in parallell with different timezones are not supported right now
    moment.tz.setDefault(timezone)
  }

  async checkoutBranch (branchName: string): Promise<TrichterDBBranch> {
    log.debug(`checkout ${branchName}`)
    await super.checkoutBranch(branchName)
    return new TrichterDBBranch(this, branchName)
  }
}
